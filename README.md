# Mitt program

## Demovideo

https://youtu.be/kGvpuqdVz6I

## Beskrivning

Spillet manøvreres ved å bruke piltastene. Spilleren kan flyttes til høyre og venstre for å fange
objektene som faller fra toppen av brettet.

Spillet er satt opp med mvc arkitektur.

Modellen behandler all logikk og holder styr på spillets tilstand.
player og board er egne klasser og ender opp i gamemodel

Visningen tegner spillet basert på modellen

Controlleren tar imot input fra brukeren og kaller på funkjsoner som manipulerer modellen.
