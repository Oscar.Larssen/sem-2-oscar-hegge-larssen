package no.uib.inf101.sem2.MoneyMan.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.MoneyBag;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.MoneyBagFactory;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.questionFactory;
import no.uib.inf101.sem2.MoneyMan.model.player.player;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class gameModelTest {

    @Test
    void testBoard() {
        gameBoard gameBoard = new gameBoard(10, 10);
        assertTrue(gameBoard.cols() == 10);
        assertTrue(gameBoard.rows() == 10);

    }

    @Test
    void testPlayer() {
        player player = new player(new CellPosition(0, 0), 'P');

        assertTrue(player.getPos().row() == 0);
        assertTrue(player.getPos().col() == 0);

        player.shiftedBy(0, 1);
        assertTrue(player.getPos().row() == 0);
        assertTrue(player.getPos().col() == 1);

        player.shiftedBy(1, 0);
        assertTrue(player.getPos().row() == 1);
        assertTrue(player.getPos().col() == 1);
    }

   

    @Test
    public void testGetTilesOnBoard() {
        gameBoard board = new gameBoard(10, 10);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);
        Iterable<GridCell<Character>> tiles = model.getTilesOnBoard();
        for (GridCell<Character> tile : tiles) {
            assertEquals('-', tile.value());
        }

    }

    @Test
    public void testGetTilesOnFallingMoneybag() {
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);
        Iterable<GridCell<Character>> tiles = model.getTilesOnFallingMoneyBag();
        for (GridCell<Character> tile : tiles) {
            assertEquals('O', tile.value());
            assertNotEquals('-', tile.value());
        }

    }

    @Test
    public void testGetDimension() {
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);
        GridDimension dimension = model.getDimension();
        assertEquals(10, dimension.rows());
        assertEquals(20, dimension.cols());
    }

    @Test
    public void testGetTilesOnPlayer() {
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);
        Iterable<GridCell<Character>> tiles = model.getTilesOnPlayer();
        for (GridCell<Character> tile : tiles) {
            assertEquals('P', tile.value());
            assertNotEquals('-', tile.value());
        }
    }

    @Test
    public void testGetTilesOnQuestion() {
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);
        Iterable<GridCell<Character>> tiles = model.getTilesOnFallingQuestion();
        for (GridCell<Character> tile : tiles) {
            assertEquals('Q', tile.value());
            assertNotEquals('-', tile.value());
        }
    }
    @Test
    public void testIslegalposition(){
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);

        MoneyBag moneyBag = (MoneyBag) model.getTilesOnFallingMoneyBag();

        assertTrue(model.isLegalNewPos(moneyBag));

        
    }

    public static void main(String[] args) {
        gameBoard board = new gameBoard(10, 20);
        MoneyBagFactory moneyFactory = new MoneyBagFactory();
        questionFactory questionFactory = new questionFactory();
        gameModel model = new gameModel(board, moneyFactory, questionFactory);

        MoneyBag moneyBag = (MoneyBag) model.getTilesOnFallingMoneyBag();

        System.out.println(moneyBag.getPos());

        moneyBag.setPos(new CellPosition(0, 23));

        System.out.println(moneyBag.getPos());

        assertFalse(model.isLegalNewPos(moneyBag));
        
    }
}
