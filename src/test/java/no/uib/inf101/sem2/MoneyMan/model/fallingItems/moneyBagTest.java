package no.uib.inf101.sem2.MoneyMan.model.fallingItems;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.MoneyMan.model.gameBoard;
import no.uib.inf101.sem2.MoneyMan.model.player.player;

public class moneyBagTest {

    @Test
    void testMoneyBag() {
        MoneyBag moneyBag = MoneyBag.newMoneyBag('O', 0);
        assertTrue(moneyBag.getPos().row() == 0);
        assertTrue(moneyBag.getPos().col() == 0);
        assertTrue(moneyBag.getSymbol() == 'O');
    }

    @Test
    void testMoneyBagFactory() {
        MoneyBagFactory moneyBagFactory = new MoneyBagFactory();
        MoneyBag moneyBag = moneyBagFactory.getNext(7);
        assertTrue(moneyBag.getPos().row() == 0);
        assertTrue(moneyBag.getPos().col() == 7);
        assertTrue(moneyBag.getSymbol() == 'O');
        assertFalse(moneyBag.getSymbol() == '-');

    }

    @Test
    void testMoneyBagMove() {

        MoneyBag moneyBag = MoneyBag.newMoneyBag('O', 0);
        moneyBag.shiftedBy(1, 0);
        assertTrue(moneyBag.getPos().row() == 0);
        assertTrue(moneyBag.getPos().col() == 0);

        MoneyBag shiftedMoneyBag = moneyBag.shiftedBy(0, 1);
        assertTrue(shiftedMoneyBag.getPos().row() == 0);
        assertTrue(shiftedMoneyBag.getPos().col() == 1);

        
    }
}
