package no.uib.inf101.sem2.MoneyMan.view;

import java.awt.Image;
import java.io.IOException;

import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public interface ViewableGameModel {

    /**
     * 
     * @return GridDimension object, antall rows and cols på brettet
     */
    public GridDimension getDimension();

    /**
     * @return et objekt som, når det itereres over,
     *         gir alle posisjonene på brettet med tilhørende symbol.
     */

    /**
     * 
     * @return an iterable of all the tiles on the board
     */
    public Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * 
     * @return an iterable of all the tiles on the falling tetromino
     */
    public Iterable<GridCell<Character>> getTilesOnFallingMoneyBag();

    /**
     * 
     * @return an iterable of all the tiles on the falling tetromino
     */
    public Iterable<GridCell<Character>> getTilesOnPlayer();

    /**
     * 
     * @return the current score of the player as a double
     */
    public double getCurrentScore();

    /**
     * 
     * @return the current HealtPoints of the player as a double
     */
    public double getCurrentHp();

    /**
     * 
     * @return an iterable of all the tiles on the falling item
     */
    public Iterable<GridCell<Character>> getTilesOnFallingQuestion();

    /**
     * 
     * @return the current GameState of the game
     */
    public GameState getGameState();

    /**
     * 
     * @return the image of the hearts used as healthpoints
     * @throws IOException
     */
    public Image getHeartImage() throws IOException;

    /**
     * 
     * @return the image of the moneybag
     * @throws IOException
     */
    public Image getMoneyImage() throws IOException;

    /**
     * 
     * @return the image of the item that takes healthpoints away
     * @throws IOException
     */
    public Image getQuestionImage() throws IOException;

    /**
     * 
     * @return the image of the player
     * @throws IOException
     */
    public Image getPlayerImage() throws IOException;

    /**
     * 
     * @return the image of the game over screen
     * @throws IOException
     */
    public Image getGameOverImage() throws IOException;


}
