package no.uib.inf101.sem2.MoneyMan.model.fallingItems;

import java.util.Random;

public class MoneyBagFactory { 

    public MoneyBag getNext(int colNumber) {
        // hentet fra stackoverflow - start
        String s = "O";
        Random random = new Random();

        int index = random.nextInt(s.length());
        Character randomShape = s.charAt(index);
        // slutt
        MoneyBag nextMoneyBag = MoneyBag.newMoneyBag(randomShape, colNumber);

        return nextMoneyBag;
    }

}