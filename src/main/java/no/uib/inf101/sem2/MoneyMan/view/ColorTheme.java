package no.uib.inf101.sem2.MoneyMan.view;

import java.awt.Color;

public interface ColorTheme {

    /**
     * 
     * @param firstCharInColor
     * @return Color object - can not be null
     */
    public Color getCellColor(Character firstCharInColor);

    /**
     * 
     * @return Color object - shoud not be null,
     *         but can be new Color(0, 0, 0, 0) = seethrough
     */
    public Color getFrameColor();

    /**
     * 
     * @return Color object - CAN NOT BE SEETHREW
     *         kan be null sets java default backround color
     * 
     */
    public Color getBackgroundColor();
}

