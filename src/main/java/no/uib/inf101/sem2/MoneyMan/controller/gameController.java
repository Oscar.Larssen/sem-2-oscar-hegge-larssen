
package no.uib.inf101.sem2.MoneyMan.controller;

import java.awt.event.KeyEvent;

import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;
import no.uib.inf101.sem2.MoneyMan.view.GameView;

import java.awt.event.ActionEvent;
import javax.swing.Timer;

public class gameController implements java.awt.event.KeyListener {

    ControllableGameModel model;
    GameView view;
    Timer timer;

    public gameController(ControllableGameModel controllableGameModel, GameView view) {

        this.model = controllableGameModel;
        this.view = view;
        this.timer = new Timer(model.getDelay(), this::clockTick);
        this.view.setFocusable(true);
        this.view.addKeyListener(this);
        this.timer.start();
    }

    public void setDelay() {

        timer.setDelay(model.getDelay());
        timer.setInitialDelay(model.getDelay());
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'keyTyped'");
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {

            model.movePlayer(0, -1);
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            model.movePlayer(0, 1);
        }

        else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            model.pauseOrPlay();

        }
        else if (e.getKeyCode() == KeyEvent.VK_R){
            model.restart();
        }

        view.repaint();

    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'keyReleased'");
    }

    public void clockTick(ActionEvent actionEvent) {

        if (model.getGameState() == GameState.ACTIVE_GAME) {
            model.clockTick();
            view.repaint();

        }
    }

}