package no.uib.inf101.sem2.MoneyMan.model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.MoneyMan.controller.ControllableGameModel;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.MoneyBag;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.MoneyBagFactory;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.questionFactory;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.question;
import no.uib.inf101.sem2.MoneyMan.model.player.player;
import no.uib.inf101.sem2.MoneyMan.view.ViewableGameModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class gameModel implements ViewableGameModel, ControllableGameModel {

    gameBoard gameBoard;
    MoneyBagFactory moneyFactory;
    MoneyBag fallingMoneybag;
    questionFactory questionFactory;
    question fallingQuestion;
    GameState Gamestate = GameState.GAME_PAUSED;
    double score;
    double Hp;
    player activePlayer;

    /**
     * Creates a new TetrisModel with the given board and factories for the falling
     * items
     * 
     * @param gameBoard
     * @param factory
     * @param questionFactory
     */
    public gameModel(gameBoard gameBoard, MoneyBagFactory factory, questionFactory questionFactory) {
        this.moneyFactory = factory;
        this.questionFactory = questionFactory;
        this.gameBoard = gameBoard;
        this.fallingMoneybag = factory.getNext(getRandomCol());
        this.fallingQuestion = questionFactory.getNext(getRandomCol());
        this.activePlayer = new player(new CellPosition(0, 0), 'P');
        this.activePlayer.startAtCenterBottomOfBoard(gameBoard);
        this.score = 0;
        this.Hp = 6;

    }

    @Override
    public GridDimension getDimension() {

        return gameBoard;

    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {

        return gameBoard;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnFallingMoneyBag() {

        return this.fallingMoneybag;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnPlayer() {

        return this.activePlayer;
    }

    public Iterable<GridCell<Character>> getTilesOnFallingQuestion() {

        return this.fallingQuestion;
    }

    @Override
    public boolean moveMoneyBag(int deltaRow, int deltaCol) {

        MoneyBag kandidat = fallingMoneybag.shiftedBy(deltaRow, deltaCol);

        if (isLegalNewPos(kandidat)) {

            fallingMoneybag = kandidat;
            return true;
        }

        return false;

    }

    /**
     * Moves the falling item down one row
     * 
     * @return boolean
     */
    private boolean moveQuestion(int deltaRow, int deltaCol) {

        question kandidat = fallingQuestion.shiftedBy(deltaRow, deltaCol);

        if (isLegalNewPos(kandidat)) {

            fallingQuestion = kandidat;
            return true;

        }

        return false;

    }

    /**
     * returns random colom on the board, so the falling item can start at a random
     * location
     * 
     * @return int
     */
    private int getRandomCol() {

        int randomCol = (int) (Math.random() * gameBoard.cols());

        return randomCol;

    }

    @Override
    public void movePlayer(int deltaRow, int deltaCol) {

        if (Gamestate == GameState.GAME_PAUSED) {
            return;
        }

        if (activePlayer.getPos().col() + deltaCol < 0
                || activePlayer.getPos().col() + deltaCol > gameBoard.cols() - 1) {
            // do nothing
        } else if (activePlayer.getPos().row() + deltaRow < 0
                || activePlayer.getPos().row() + deltaRow > gameBoard.rows() - 2) {
            // do nothing
        } else {
            activePlayer.shiftedBy(deltaRow, deltaCol);
        }

    }

    /**
     * Checks if the new position of the falling item is legal
     * 
     * @param kandidat - the new item position
     * @return boolean
     */
    boolean isLegalNewPos(MoneyBag kandidat) {

        checkForOverlapWithPlayer();

        for (GridCell<Character> gridCell : kandidat) {
            if (!gameBoard.positionIsOnGrid(gridCell.pos())) {
                return false;
            }

            if (gameBoard.get(gridCell.pos()) != '-') {
                checkForOverlapWithPlayer();
            }

        }
        return true;
    }

    /**
     * Checks if the new position of the falling item is legal
     * 
     * @param kandidat - the new item to be checked
     * @return boolean
     */
    private boolean isLegalNewPos(question kandidat) {

        checkForOverlapWithPlayer();

        for (GridCell<Character> gridCell : kandidat) {
            if (!gameBoard.positionIsOnGrid(gridCell.pos())) {
                return false;
            }

            if (gameBoard.get(gridCell.pos()) != '-') {
                checkForOverlapWithPlayer();
            }

        }
        return true;
    }

    private void getNewFallingMoneyBag() {

        MoneyBag kandidat = moneyFactory.getNext(getRandomCol());

        if (isLegalNewPos(kandidat)) {
            fallingMoneybag = kandidat;
        } else {

        }

    }

    private void getNewFallingQuestion() {
        question kandidat = questionFactory.getNext(getRandomCol());
        fallingQuestion = kandidat;

    }

    /**
     * Checks if the falling item overlaps with the player
     * increases score if it
     * 
     * @return boolean
     */
    private void checkForOverlapWithPlayer() {

        if (Gamestate == GameState.GAME_OVER) {
            return;
        }

        if (fallingMoneybag.getSymbol() == 'O') {
            for (GridCell<Character> gridCell : fallingMoneybag) {
                if (activePlayer.getPos().equals(gridCell.pos())) {
                    gameBoard.set(gridCell.pos(), '-');
                    score += 0.5;
                    
                } else {

                }
            }
        }
        if (fallingQuestion.getCharSymbol() == 'Q') {
            for (GridCell<Character> gridCell : fallingQuestion) {
                if (activePlayer.getPos().equals(gridCell.pos())) {
                    gameBoard.set(gridCell.pos(), '-');
                    updateHp();
                   // Hp -= 1;
                } else {

                }
            }
        }

    }

    private void updateHp() {

        if (fallingQuestion.getCharSymbol() == 'Q') {
            for (GridCell<Character> gridCell : fallingQuestion) {
                if (activePlayer.getPos().equals(gridCell.pos())) {
                    gameBoard.set(gridCell.pos(), '-');
                    Hp -= 1;
                } else {

                }
            }
        }

    }

   
    @Override
    public GameState getGameState() {

        if (getCurrentHp() <= 0) {
            Gamestate = GameState.GAME_OVER;
        }
        return Gamestate;
    }

    @Override
    public int getDelay() {
        return 100;
    }

    @Override
    public void clockTick() {

        if (Gamestate == GameState.GAME_OVER) {
            return;
        }

        if (Gamestate == GameState.GAME_PAUSED) {
            return;
        }

        if (moveMoneyBag(1, 0)) {
        } else {
            getNewFallingMoneyBag();

        }

        if (moveQuestion(1, 0)) {
        } else {
            getNewFallingQuestion();
        }

    }

    @Override
    public double getCurrentScore() {

        return score;
    }

    @Override
    public double getCurrentHp() {

        if (Hp < 0) {
            Gamestate = GameState.GAME_OVER;
        }
        return Hp;
    }

    @Override
    public void pauseOrPlay() {

        if (Gamestate == GameState.GAME_PAUSED) {
            Gamestate = GameState.ACTIVE_GAME;
        } else {
            Gamestate = GameState.GAME_PAUSED;
        }

    }

    @Override
    public Image getHeartImage() throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/sitsteForsøkHjerte.png"));
        return image;

    }

    @Override
    public Image getMoneyImage() throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/storPengebag-removebg-preview.png"));
        return image;
    }

    @Override
    public Image getQuestionImage() throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/rund solvang.png"));
        return image;
    }

    @Override
    public Image getPlayerImage() throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/trygve_animert-removebg-preview.png"));
        return image;

    }

    @Override
    public Image getGameOverImage() throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/gameOverTrygve.png"));
        // make the image easier to load

        return image;

    }

    @Override
    public void restart() {
        if (Gamestate == GameState.GAME_OVER) {

            Gamestate = GameState.ACTIVE_GAME;
            gameBoard = new gameBoard(15, 14);
            activePlayer = new player(new CellPosition(0, 0), 'P');
            activePlayer.startAtCenterBottomOfBoard(gameBoard);
            moneyFactory = new MoneyBagFactory();
            fallingMoneybag = moneyFactory.getNext(getRandomCol());
            fallingQuestion = questionFactory.getNext(getRandomCol());
            score = 0;
            Hp = 6;
        } else {
            return;

        }
    }

    public player getPlayer() {
        return activePlayer;
    }

}
