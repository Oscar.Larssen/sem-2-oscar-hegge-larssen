package no.uib.inf101.sem2.MoneyMan.model.fallingItems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public final class MoneyBag implements Iterable<GridCell<Character>> {
    private Character symbol;
    private boolean[][] shape;
    private CellPosition pos;

    private MoneyBag(Character symbol, boolean[][] shape, CellPosition LeftCornerposition) {
        this.symbol = symbol;
        this.shape = shape;
        this.pos = LeftCornerposition;

    }

    static MoneyBag newMoneyBag(Character shapeChar, int colNumber) throws IllegalArgumentException {

        CellPosition pos = new CellPosition(0, colNumber);

        switch (shapeChar) {

            case 'O':
                boolean[][] O = new boolean[][] {
                        { false, false, false },
                        { false, true, false },
                        { false, false, false }
                };
                return new MoneyBag(shapeChar, O, pos);
        }
        throw new IllegalArgumentException("Invalid shape character: " + shapeChar);

    }

    public MoneyBag shiftedBy(int deltarow, int deltacol) {

        int newRow = this.pos.row() + deltarow;
        int newCol = this.pos.col() + deltacol;

        return new MoneyBag(this.symbol, this.shape, new CellPosition(newRow, newCol));

    }

    @Override
    public Iterator<GridCell<Character>> iterator() {

        ArrayList<GridCell<Character>> posInShape = new ArrayList<GridCell<Character>>();

        for (int row = 0; row < this.shape.length; row++) {

            for (int col = 0; col < this.shape[0].length; col++) {
                if (this.shape[row][col]) {
                    posInShape.add(new GridCell<Character>(new CellPosition(this.pos.row() + row, this.pos.col() + col),
                            this.symbol));
                }
            }

        }
        return posInShape.iterator();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
        result = prime * result + Arrays.deepHashCode(shape);
        result = prime * result + ((pos == null) ? 0 : pos.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MoneyBag other = (MoneyBag) obj;
        if (symbol == null) {
            if (other.symbol != null)
                return false;
        } else if (!symbol.equals(other.symbol))
            return false;
        if (!Arrays.deepEquals(shape, other.shape))
            return false;
        if (pos == null) {
            if (other.pos != null)
                return false;
        } else if (!pos.equals(other.pos))
            return false;
        return true;
    }

    public Character getSymbol() {
        return symbol;
    }

    public CellPosition getPos() {
        return pos;
    }
    public void setPos(CellPosition pos){
        this.pos = pos;
    }

}
