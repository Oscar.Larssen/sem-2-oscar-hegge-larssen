package no.uib.inf101.sem2.MoneyMan.model.fallingItems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public final class question implements Iterable<GridCell<Character>> {
    private Character charSymbol;
    private boolean[][] booleanListShape;
    private CellPosition pos;

    private question(Character symbol, boolean[][] shape, CellPosition LeftCornerposition) {
        this.charSymbol = symbol;
        this.booleanListShape = shape;
        this.pos = LeftCornerposition;

    }

    static question newQuestion(Character shapeChar, int colNumber) throws IllegalArgumentException {

        CellPosition pos = new CellPosition(0, colNumber);

        switch (shapeChar) { // switch in case we want to add more shapes

            case 'Q':
                boolean[][] Q = new boolean[][] {
                        { false, true, false },
                };
                return new question(shapeChar, Q, pos);
        }
        throw new IllegalArgumentException("Invalid shape character: " + shapeChar);

    }

    public question shiftedBy(int deltarow, int deltacol) {

        int newRow = this.pos.row() + deltarow;
        int newCol = this.pos.col() + deltacol;

        return new question(this.charSymbol, this.booleanListShape, new CellPosition(newRow, newCol));

    }

    @Override
    public Iterator<GridCell<Character>> iterator() {

        ArrayList<GridCell<Character>> posInShape = new ArrayList<GridCell<Character>>();

        for (int row = 0; row < this.booleanListShape.length; row++) {

            for (int col = 0; col < this.booleanListShape[0].length; col++) {
                if (this.booleanListShape[row][col]) {
                    posInShape.add(new GridCell<Character>(new CellPosition(this.pos.row() + row, this.pos.col() + col),
                            this.charSymbol));
                }
            }

        }
        return posInShape.iterator();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((charSymbol == null) ? 0 : charSymbol.hashCode());
        result = prime * result + Arrays.deepHashCode(booleanListShape);
        result = prime * result + ((pos == null) ? 0 : pos.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        question other = (question) obj;
        if (charSymbol == null) {
            if (other.charSymbol != null)
                return false;
        } else if (!charSymbol.equals(other.charSymbol))
            return false;
        if (!Arrays.deepEquals(booleanListShape, other.booleanListShape))
            return false;
        if (pos == null) {
            if (other.pos != null)
                return false;
        } else if (!pos.equals(other.pos))
            return false;
        return true;
    }

    public Character getCharSymbol() {
        return charSymbol;
    }

    public boolean[][] getBooleanListShape() {
        return booleanListShape;
    }

    public CellPosition getPos() {
        return pos;
    }

    public void setCharSymbol(Character charSymbol) {
        this.charSymbol = charSymbol;
    }

    public void setBooleanListShape(boolean[][] booleanListShape) {
        this.booleanListShape = booleanListShape;
    }

    public void setPos(CellPosition pos) {
        this.pos = pos;
    }

}
