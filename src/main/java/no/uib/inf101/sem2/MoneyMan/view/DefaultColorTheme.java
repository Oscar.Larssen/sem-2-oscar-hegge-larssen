package no.uib.inf101.sem2.MoneyMan.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

    public DefaultColorTheme() {

    }

    @Override
    public Color getCellColor(Character c) {
        Color color = switch (c) {

            case 'O' -> Color.YELLOW;

            case 'P' -> Color.RED;

            case 'Q' -> Color.black;

            case '-' -> new Color(135, 206, 235);
            default -> throw new IllegalArgumentException(
                    "No available color for '" + c + "'");
        };
        return color;

    }

    @Override
    public Color getFrameColor() {
        return new Color(128, 186, 41);
    }

    @Override
    public Color getBackgroundColor() {
        return new Color(255, 255, 255);

    }

    

}
