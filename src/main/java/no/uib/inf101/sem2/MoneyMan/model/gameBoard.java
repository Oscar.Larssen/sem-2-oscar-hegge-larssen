package no.uib.inf101.sem2.MoneyMan.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

public class gameBoard extends Grid<Character> {

    private int currentScore;

    /**
     * Creates a new TetrisBoard with the given dimensions.
     * 
     * @param rows
     *             The number of rows
     * @param cols
     *             The number of columns
     */
    public gameBoard(int rows, int cols) {
        super(rows, cols, '-');
        currentScore = 0;

    }

    public String getAmountOfCurrentMoneyAsString() {
        String stringScore = Integer.toString(currentScore);

        return stringScore;
    }

    public String prettyString() {
        String line = "";
        String fullPrettyString = "";

        for (int row = 0; row < this.rows(); row++) {

            for (int col = 0; col < this.cols(); col++) {
                line += this.get(new CellPosition(row, col));
            }
            fullPrettyString += line;
            if (row < this.rows() - 1) {
                fullPrettyString += "\n";
            }
            line = "";

        }
        return fullPrettyString;
    }

}
