package no.uib.inf101.sem2.MoneyMan.model.fallingItems;


public enum GameState {
    ACTIVE_GAME, GAME_OVER, GAME_PAUSED
}
