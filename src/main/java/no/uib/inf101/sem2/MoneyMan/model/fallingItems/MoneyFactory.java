package no.uib.inf101.sem2.MoneyMan.model.fallingItems;


    
public interface MoneyFactory {

    /*
     * returns a new falling object
     * @param colNumber the column number of the new falling object
     * 
     */

    public MoneyBag getNext(int colNumber);
}
