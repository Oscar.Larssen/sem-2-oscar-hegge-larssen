package no.uib.inf101.sem2.MoneyMan.view;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;
import no.uib.inf101.sem2.grid.GridCell;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class GameView extends JPanel {

  ViewableGameModel model;
  ColorTheme color;

  public GameView(ViewableGameModel model) {
    this.color = new DefaultColorTheme();
    this.model = model;
    this.setBackground(new Color(135, 206, 235));
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(model.getDimension().cols() * 80, 700));

  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    if (model.getGameState() == GameState.GAME_PAUSED) {
      drawPauseScreen(g2);
    } else if (model.getGameState() == GameState.GAME_OVER) {
      drawGameOver(g2);
    } else {
      drawGame(g2);
    }

  }

  public void drawGame(Graphics2D g2) {

    double margin = 0;
    double topMargin = 50;
    double grass = 20;
    double x = margin;
    double y = topMargin;
    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - grass - topMargin;
    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);

    BufferedImage backgroundImage = null;

    try {
      backgroundImage = ImageIO.read(new File("src/main/resources/bredere skog 2023-04-18 kl. 15.17.22.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    g2.drawImage(backgroundImage, 0, 0, null);

    g2.setColor(new Color(255, 215, 0));

    g2.setFont(new Font("Arial", Font.BOLD, 40));
    int scoreWidth = g2.getFontMetrics().stringWidth(" $" + model.getCurrentScore());
    g2.drawString(model.getCurrentScore() + " $", (int) (x + width / 2 - scoreWidth / 2), 110);

    if (model.getCurrentHp() >= 1) {
      try {
        g2.drawImage(model.getHeartImage(), (int) (x + width / 2 - 40), 30, 30, 30, null);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    if (model.getCurrentHp() >= 3) {
      try {
        g2.drawImage(model.getHeartImage(), (int) (x + width / 2), 30, 30, 30, null);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    if (model.getCurrentHp() >= 6) {

      // draw a small image
      try {
        g2.drawImage(model.getHeartImage(), (int) (x + width / 2 + 40), 30, 30, 30, null);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }

    CellPositionToPixelConverter cellPosToPix = new CellPositionToPixelConverter(rectangle, model.getDimension(),
        margin);

    drawCells(g2, model.getTilesOnBoard(), cellPosToPix, this.color);
    drawCells(g2, model.getTilesOnFallingMoneyBag(), cellPosToPix, this.color);
    drawCells(g2, model.getTilesOnPlayer(), cellPosToPix, this.color);
    drawCells(g2, model.getTilesOnFallingQuestion(), cellPosToPix, this.color);
  }

  public void drawCells(Graphics2D canvas, Iterable<GridCell<Character>> iterable,
      CellPositionToPixelConverter cellPosToPixelConv, ColorTheme color) {

    for (GridCell<Character> cell : iterable) {

      Rectangle2D rectangle = cellPosToPixelConv.getBoundsForCell(cell.pos());

      if (cell.value() == 'P') {
        try {
          canvas.drawImage(model.getPlayerImage(), (int) rectangle.getX(), (int) rectangle.getY(), (int) 100,
              (int) 100, null);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

      } else if (cell.value() == 'O') {
        try {
          canvas.drawImage(model.getMoneyImage(), (int) rectangle.getX(), (int) rectangle.getY(), (int) 100,
              (int) 100, null);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

      } else if (cell.value() == 'Q') {

        try {
          canvas.drawImage(model.getQuestionImage(), (int) rectangle.getX(), (int) rectangle.getY(), (int) 100,
              (int) 100, null);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

    }

  }

  public void drawPauseScreen(Graphics2D g2) {

    BufferedImage backgroundImage = null;

    try {
      backgroundImage = ImageIO.read(new File("src/main/resources/bredere skog 2023-04-18 kl. 15.17.22.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
    g2.drawImage(backgroundImage, 0, 0, null);
    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

    g2.setColor(Color.black);
    g2.setFont(new Font("Verdana", Font.BOLD, 40));
    int titleWidth = g2.getFontMetrics().stringWidth("MoneyManTrygve");
    g2.drawString("MoneyManTrygve", (int) (this.getWidth() / 2 - titleWidth / 2), this.getHeight() / 2 - 250);

    g2.setColor(Color.black);
    g2.setFont(new Font("Arial", Font.BOLD, 20));
    String pauseText = "Trygve Slagsmål Vedum vil gjerne fange pengesekkene men helst ikke stille på debatten. \n Bruk PILENE <- ->for å fange mest mulig penger og unngå Fredrik Solvang \n Trykk på enter for å starte/Fortsette spillet";
    String[] lines = pauseText.split("\n");
    int lineHeight = g2.getFontMetrics().getHeight();
    int totalHeight = lines.length * lineHeight;
    int startY = this.getHeight() / 2 - totalHeight / 2;

    for (int i = 0; i < lines.length; i++) {
      int lineWidth = g2.getFontMetrics().stringWidth(lines[i]);
      int startX = this.getWidth() / 2 - lineWidth / 2;
      int lineY = startY + i * lineHeight;
      g2.drawString(lines[i], startX, lineY);
    }

    g2.setColor(new Color(255, 215, 0));
    g2.setFont(new Font("Arial", Font.BOLD, 80));
    int scoreWidth = g2.getFontMetrics().stringWidth(" $ " + model.getCurrentScore());
    g2.drawString(model.getCurrentScore() + " $ ", (int) (this.getWidth() / 2 - scoreWidth / 2),
        this.getHeight() / 2 + 150);
  }

  public void drawGameOver(Graphics2D g2) {
    // draw the string "press R to restart"
    g2.setColor(Color.white);
    g2.setFont(new Font("Arial", Font.BOLD, 20));
    String restartText = "Press R to restart";
    int restartTextWidth = g2.getFontMetrics().stringWidth(restartText);
    int restartCenterX = getWidth() / 2;
    g2.drawString(restartText, restartCenterX - restartTextWidth / 2, 40);

    g2.setColor(Color.WHITE);
    g2.setFont(new Font("Arial", Font.BOLD, 150));
    String gameOverText = "Game Over";
    int gameOverTextWidth = g2.getFontMetrics().stringWidth(gameOverText);
    int centerX = getWidth() / 2;
    int centerY = getHeight() / 2 - 40;
    g2.drawString(gameOverText, centerX - gameOverTextWidth / 2, centerY);

    g2.setColor(new Color(255, 215, 0));
    // gold color rgb 255, 215, 0

    g2.setFont(new Font("Arial", Font.BOLD, 30));
    String scoreText = model.getCurrentScore() + " $ ";
    int scoreTextWidth = g2.getFontMetrics().stringWidth(scoreText);
    int scoreCenterY = centerY + 40;
    g2.drawString(scoreText, centerX - scoreTextWidth / 2, scoreCenterY);

    try {
      Image gameOverImage = model.getGameOverImage();
      int imageX = centerX - 150;
      int imageY = centerY + 60;
      g2.drawImage(gameOverImage, imageX, imageY, 300, 300, null);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
