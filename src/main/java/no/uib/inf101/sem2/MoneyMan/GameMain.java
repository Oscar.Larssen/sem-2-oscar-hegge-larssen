package no.uib.inf101.sem2.MoneyMan;

import javax.swing.JFrame;

import no.uib.inf101.sem2.MoneyMan.controller.gameController;
import no.uib.inf101.sem2.MoneyMan.model.gameBoard;
import no.uib.inf101.sem2.MoneyMan.model.gameModel;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.MoneyBagFactory;
import no.uib.inf101.sem2.MoneyMan.model.fallingItems.questionFactory;
import no.uib.inf101.sem2.MoneyMan.view.GameView;

public class GameMain {
  public static final String WINDOW_TITLE = "MoneyMan";

  public static void main(String[] args) {

    gameBoard board = new gameBoard(15, 14);

    MoneyBagFactory moneyBagFactory = new MoneyBagFactory();
    questionFactory questionFactory = new questionFactory();
    gameModel model = new gameModel(board, moneyBagFactory, questionFactory);
    GameView view = new GameView(model);

    gameController controller = new gameController(model, view);

    JFrame frame = new JFrame(WINDOW_TITLE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    frame.setContentPane(view);

    frame.pack();
    frame.setVisible(true);

  }

}
