package no.uib.inf101.sem2.MoneyMan.model.player;

import java.util.ArrayList;
import java.util.Iterator;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class player implements Iterable<GridCell<Character>> {

    private CellPosition pos;
    private Character symbol;
    private boolean[][] shape;

    public player(CellPosition position, Character symbol) {
        this.pos = position;
        this.symbol = symbol;
        this.shape = new boolean[][] {

                { false },
                { true }
        };

    }

    public CellPosition getPos() {
        return pos;
    }

    public void setPos(CellPosition position) {
        this.pos = position;
    }

    public void shiftedBy(int deltarow, int deltacol) {

        int newRow = this.pos.row() + deltarow;
        int newCol = this.pos.col() + deltacol;

        this.pos = new CellPosition(newRow, newCol);

    }

    /**
     * Sets the player at the center bottom of the board
     * 
     * @param gridDimension
     */
    public void startAtCenterBottomOfBoard(GridDimension gridDimension) {

        pos = new CellPosition(gridDimension.rows() - 6, gridDimension.cols() / 2);

    }

    @Override
    public Iterator<GridCell<Character>> iterator() {

        ArrayList<GridCell<Character>> posInShape = new ArrayList<GridCell<Character>>();

        for (int row = 0; row < this.shape.length; row++) {

            for (int col = 0; col < this.shape[0].length; col++) {
                if (this.shape[row][col]) {
                    posInShape.add(new GridCell<Character>(new CellPosition(this.pos.row() + row, this.pos.col() + col),
                            this.symbol));
                }
            }

        }
        return posInShape.iterator();

    }

}
