package no.uib.inf101.sem2.MoneyMan.controller;

import no.uib.inf101.sem2.MoneyMan.model.fallingItems.GameState;

public interface ControllableGameModel {

  /**
   * 
   * @param int deltaRow
   * @param int deltaCol
   * @return true hvis flyttingen ble gjennomført, false hvis ikke
   * 
   *         skal kunne brukes for å flytte brikken rundt på brettet
   */
  public boolean moveMoneyBag(int deltaRow, int deltaCol);

  /**
   * 
   * @param int deltaRow
   * @param int deltaCol
   * 
   *            skal kunne brukes for å flytte spilleren rundt på brettet
   *            flytter anntall rader og kolonner som deltaRow og deltaCol angir
   */
  public void movePlayer(int deltaRow, int deltaCol);

  /**
   * 
   * @return gamestate
   */
  public GameState getGameState();

  /**
   * 
   * @return hvor mange millisekunder (som integer) det skal være mellom hvert
   *         klokkeslag
   */
  public int getDelay();

  /**
   * kalles hver gang klokken tikker. se metoden getDelay() for frekvens
   */
  public void clockTick();

  /**
   * 
   * endrer gamestate for å skille med pause og play
   */
  public void pauseOrPlay();

public void restart();

}
