package no.uib.inf101.sem2.MoneyMan.view;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {

    Rectangle2D box;
    GridDimension gd;
    double margin;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {

        this.box = box;
        this.gd = gd;
        this.margin = margin;

    }

    public Rectangle2D getBoundsForCell(CellPosition cellPosition) {

        double CellWidth = ((box.getWidth() - (gd.cols() + 1) * margin)) / (gd.cols());

        double CellX = box.getX() + margin + ((margin + CellWidth) * cellPosition.col());

        double CellHeight = (box.getHeight() - ((gd.rows() + 1) * margin)) / gd.rows();

        double CellY = box.getY() + (margin + (margin + CellHeight) * cellPosition.row());

        Rectangle2D.Double rectangle = new Rectangle2D.Double(CellX, CellY, CellWidth, CellHeight);

        return rectangle;
    }
}
