package no.uib.inf101.sem2.grid;


import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {

    int rows;
    int cols;

    E E_object;

    ArrayList<ArrayList<E>> grid;
    // selve gridden. todimensjonell liste

    public Grid(int rows, int cols) {

        this.rows = rows;
        this.cols = cols;

        grid = new ArrayList<>();

        for (int r = 0; r < rows; r++) {
            // oppretter anntall lister i rows
            grid.add(new ArrayList<E>());
            // for hver rad/liste legges det til 'null' * Cols
            //
            for (int c = 0; c < cols; c++) {

                this.grid.get(r).add(null);

            }
        }
    }

    public Grid(int rows, int cols, E defaultValue) {

        this.rows = rows;
        this.cols = cols;

        grid = new ArrayList<>();

        for (int r = 0; r < rows; r++) {
            // oppretter anntall lister i rows
            grid.add(new ArrayList<E>());
            // for hver rad/liste legges det til 'null' * Cols
            //
            for (int c = 0; c < cols; c++) {

                this.grid.get(r).add(defaultValue);

            }
        }
    }

    @Override
    public int rows() {

        return grid.size();
    }

    @Override
    public int cols() {

        return grid.get(0).size();
    }

    @Override
    public Iterator<GridCell<E>> iterator() {

        ArrayList<GridCell<E>> cellObjects = new ArrayList<GridCell<E>>();

        for (int r = 0; r < grid.size(); r++) {

            for (int c = 0; c < grid.get(0).size(); c++) {

                E_object = (grid.get(r).get(c));

                cellObjects.add(new GridCell<E>(new CellPosition(r, c), E_object));
            }

        }

        return cellObjects.iterator();

    }

    @Override
    public void set(CellPosition pos, E value) throws IndexOutOfBoundsException {
        int r = pos.row();
        int c = pos.col();
        grid.get(r).set(c, value);
    }

    @Override
    public E get(CellPosition pos) throws IndexOutOfBoundsException {

        return grid.get(pos.row()).get(pos.col());
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        // sjekke at ikke row eller col er høyere eller lavere

        if (pos.col() < 0)
            return false;
        if (pos.row() < 0)
            return false;
        if (pos.row() >= this.rows - 4)
            return false;
        if (pos.col() >= this.cols)
            return false;
        return true;

    }

}
